import {
    Construct,
    Duration,
    RemovalPolicy,
    Stack,
    StackProps,
} from '@aws-cdk/core';
import {
  PolicyDocument,
  PolicyStatement,
  Role,
  ServicePrincipal,
} from '@aws-cdk/aws-iam';
import { Code, Function as LambdaFunction, Runtime } from '@aws-cdk/aws-lambda';
import { SnsEventSource } from '@aws-cdk/aws-lambda-event-sources';
import { RetentionDays } from '@aws-cdk/aws-logs';
import { Topic } from '@aws-cdk/aws-sns';
import { Bucket } from '@aws-cdk/aws-s3';

export default class AmazonRekognitionStack extends Stack {
  public constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const bucket = new Bucket(this, 'RekognitionBucket', {
      bucketName: `rekognition-medium-material`,
      removalPolicy: RemovalPolicy.DESTROY,
    });

    const topic = new Topic(this, 'LabelDetectionTopic', {
      topicName: 'rekognition-label-detection',
    });

    const bucketObjectsArn = `${bucket.bucketArn}/*`;

    const s3PolicyStatement = new PolicyStatement({
      actions: [
        's3:GetObject',
        's3:GetObjectTagging',
      ],
      resources: [bucketObjectsArn],
    });

    const rekognitionPolicyStatement = new PolicyStatement({
      actions: [
        'rekognition:CompareFaces',
        'rekognition:DetectFaces',
        'rekognition:DetectLabels',
        'rekognition:GetLabelDetection',
        'rekognition:StartLabelDetection',
      ],
    });

    rekognitionPolicyStatement.addAllResources();

    const snsPolicyStatement = new PolicyStatement({
      actions: ['sns:Publish'],
      resources: [topic.topicArn],
    });

    const snsPolicyDocument = new PolicyDocument({
      statements: [snsPolicyStatement],
    });

    const snsRole = new Role(this, 'SNSRole', {
      assumedBy: new ServicePrincipal('rekognition.amazonaws.com'),
      inlinePolicies: {
        snsPolicy: snsPolicyDocument,
      },
    });

    const iamPolicyStatement = new PolicyStatement({
      actions: ['iam:PassRole'],
      resources: [snsRole.roleArn],
    });

    const lambdaFunction = new LambdaFunction(this, 'RekognitionFunction', {
      code: Code.asset('./src'),
      description: 'Rekognition functionality for image and video analysis.',
      environment: {
        MAX_LABELS: '100',
        MIN_CONFIDENCE: '70.0',
        SNS_ROLE_ARN: snsRole.roleArn,
        SNS_TOPIC_ARN: `${topic.topicArn}`,
        S3_BUCKET: bucket.bucketName,
      },
      functionName: 'rekognition-analyser',
      handler: 'rekognition.lambda_handler',
      initialPolicy: [
        iamPolicyStatement,
        rekognitionPolicyStatement,
        s3PolicyStatement,
      ],
      logRetention: RetentionDays.ONE_WEEK,
      runtime: Runtime.PYTHON_3_8,
      timeout: Duration.seconds(5),
    });

    const snsEventSource = new SnsEventSource(topic);

    lambdaFunction.addEventSource(snsEventSource);
  }
}
