# Rekognition Analyser
## Setup
The required resources and permissions are built and deployed using the [AWS Cloud Development Kit (AWS CDK)](https://aws.amazon.com/cdk/).

This stack consists of a Lambda function, an S3 bucket for images and video, and an SNS topic for the video analysis.

### Build
```
npm run build
```

### Deploy
```
cdk deploy
```

### Destroy
```
cdk destroy
```

## Execution
This Lambda function can be executed using the AWS Console. Test event templates provided below.

## Detecting Labels (Image)
```
{
  "eventType": "detectObjects",
  "sourceKey": <insert source S3 key>
}
```

## Detecting Faces (Image)
```
{
  "eventType": "facialRecognition",
  "sourceKey": <insert source S3 key>
}
```

## Comparing Faces (Images)
```
{
  "eventType": "compareFaces",
  "sourceKey": <insert source S3 key>,
  "targetKey": <insert target S3 key>
}
```

## Detecting Labels (Video)
```
{
  "eventType": "startVideoDetection",
  "sourceKey": <insert source S3 key>
}
```
