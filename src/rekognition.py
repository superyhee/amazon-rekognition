import boto3
import json
import logging
import os

from botocore.exceptions import ClientError
from http import HTTPStatus

MAX_LABELS = os.getenv("MAX_LABELS")
MIN_CONFIDENCE = os.getenv("MIN_CONFIDENCE")
SNS_ROLE_ARN = os.getenv("SNS_ROLE_ARN")
SNS_TOPIC_ARN = os.getenv("SNS_TOPIC_ARN")
S3_BUCKET = os.getenv("S3_BUCKET")

LOG = logging.getLogger()
REKOGNITION_CLIENT = boto3.client("rekognition")

SOURCE_KEY = "sourceKey"

LOG.setLevel(logging.INFO)


def lambda_handler(event: dict, context) -> dict:
    if "Records" in event and len(event["Records"]):
        LOG.info("Triggered automatically by SNS")
        return get_video_label_detection(event["Records"])

    try:
        if event["eventType"] == "compareFaces":
            return compare_faces(event)
        elif event["eventType"] == "detectObjects":
            return detect_objects_and_scenes(event)
        elif event["eventType"] == "startVideoDetection":
            return start_video_label_detection(event)
        elif event["eventType"] == "facialRecognition":
            return facial_recognition(event)

        err_message = f"Event type '{event['eventType']}' not recognised."
        LOG.error(err_message)
        return generate_response(HTTPStatus.BAD_REQUEST, err_message)
    except KeyError as err:
        return generate_response(HTTPStatus.BAD_REQUEST, str(err))


def generate_response(status_code: int, body: str, headers: dict = {}) -> dict:
    LOG.info("Generating response")
    return {
        "statusCode": status_code,
        "headers": headers,
        "body": body,
        "isBase64Encoded": True
    }


def compare_faces(event: dict) -> dict:
    LOG.info("Comparing faces recognised between two images")
    try:
        response = REKOGNITION_CLIENT.compare_faces(
            SourceImage={
                'S3Object': {
                    'Bucket': S3_BUCKET,
                    'Name': event[SOURCE_KEY]
                }
            },
            TargetImage={
                'S3Object': {
                    'Bucket': S3_BUCKET,
                    'Name': event["targetKey"]
                }
            }
        )
    except ClientError as err:
        LOG.error("Failed to compare faces for the given images")
        return generate_response(HTTPStatus.INTERNAL_SERVER_ERROR, str(err))
    except (KeyError, ValueError) as err:
        LOG.error("Failed to compare faces for the given images")
        return generate_response(HTTPStatus.BAD_REQUEST, str(err))

    if "FaceMatches" in response:
        return generate_response(
            HTTPStatus.OK,
            json.dumps(response["FaceMatches"])
        )


def detect_objects_and_scenes(event: dict) -> dict:
    LOG.info("Detecting objects and scenes in the given image")
    try:
        response = REKOGNITION_CLIENT.detect_labels(
            Image={
                "S3Object": {
                    "Bucket": S3_BUCKET,
                    "Name": event[SOURCE_KEY]
                }
            },
            MaxLabels=int(MAX_LABELS),
            MinConfidence=float(MIN_CONFIDENCE)
        )
    except ClientError as err:
        LOG.error("Failed to detect labels for the given image")
        return generate_response(HTTPStatus.INTERNAL_SERVER_ERROR, str(err))
    except (KeyError, ValueError) as err:
        LOG.error("Failed to detect labels for the given image")
        return generate_response(HTTPStatus.BAD_REQUEST, str(err))

    if "Labels" in response:
        return generate_response(HTTPStatus.OK, json.dumps(response["Labels"]))


def facial_recognition(event: dict) -> dict:
    LOG.info("Detecting faces in the provided image")
    try:
        response = REKOGNITION_CLIENT.detect_faces(
            Image={
                "S3Object": {
                    "Bucket": S3_BUCKET,
                    "Name": event[SOURCE_KEY]
                }
            },
            Attributes=['ALL']
        )
    except ClientError as err:
        LOG.error("Failed to detect faces for the given image")
        return generate_response(HTTPStatus.INTERNAL_SERVER_ERROR, str(err))
    except (KeyError, ValueError) as err:
        LOG.error("Failed to detect faces for the given image")
        return generate_response(HTTPStatus.BAD_REQUEST, str(err))

    if "FaceDetails" in response:
        return generate_response(
            HTTPStatus.OK,
            json.dumps(response["FaceDetails"])
        )


def get_video_label_detection(records: [dict]) -> dict:
    LOG.info(f"Iterating over {len(records)} SNS message(s)")
    responses = []

    for record in records:
        message = json.loads(record["Sns"]["Message"])

        if message["Status"] == "SUCCEEDED":
            LOG.info("Retrieving video label detection details from Rekognition")
            try:
                response = REKOGNITION_CLIENT.get_label_detection(
                        JobId=message["JobId"],
                        MaxResults=int(MAX_LABELS),
                )
            except ClientError as err:
                LOG.error("Failed to start label detection for the given video")
                return generate_response(HTTPStatus.INTERNAL_SERVER_ERROR, str(err))
            except (KeyError, ValueError) as err:
                LOG.error("Failed to start label detection for the given video")
                return generate_response(HTTPStatus.BAD_REQUEST, str(err))

            responses.append(response)
        else:
            LOG.error("Error response received from SNS")
    print(responses)


def start_video_label_detection(event: dict) -> dict:
    LOG.info("Starting label detection for given video")
    try:
        response = REKOGNITION_CLIENT.start_label_detection(
            Video={
                "S3Object": {
                    "Bucket": S3_BUCKET,
                    "Name": event[SOURCE_KEY]
                }
            },
            MinConfidence=float(MIN_CONFIDENCE),
            NotificationChannel={
                "SNSTopicArn": SNS_TOPIC_ARN,
                "RoleArn": SNS_ROLE_ARN,
            }
        )
    except ClientError as err:
        LOG.error("Failed to start label detection for the given video")
        return generate_response(HTTPStatus.INTERNAL_SERVER_ERROR, str(err))
    except (KeyError, ValueError) as err:
        LOG.error("Failed to start label detection for the given video")
        return generate_response(HTTPStatus.BAD_REQUEST, str(err))

    if "JobId" in response:
        return generate_response(HTTPStatus.OK, response["JobId"])
