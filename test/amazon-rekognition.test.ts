import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import { App } from '@aws-cdk/core';
import AmazonRekognitionStack from '../lib/amazon-rekognition-stack';

test('Empty Stack', () => {
    const app = new App();
    // WHEN
    const stack = new AmazonRekognitionStack(app, 'MyTestStack');
    // THEN
    expectCDK(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});
